#
# comms.coffee -- wrap the receive/send part
#

net        = require 'net'
JSONStream = require 'JSONStream'

log = console.log

serverHost = process.argv[2]
serverPort = process.argv[3]
botName    = process.argv[4]
botKey     = process.argv[5]

client = false

exports.send = (json) ->
	if client
		client.write JSON.stringify(json)
		client.write '\n'

exports.enable = (handler, cb) ->
	log "I'm #{botName} and connect to #{serverHost}:#{serverPort}"

	client = net.connect serverPort, serverHost, cb

	jsonStream = client.pipe JSONStream.parse()

	jsonStream.on 'data', (data) ->
		handler[data.msgType]? data

	jsonStream.on 'error', ->
		console.log "disconnected"
