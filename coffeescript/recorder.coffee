#
# recorder.coffee -- lap data recorder
#

_  = require 'underscore'
fs = require 'fs'

log = console.log

class Recorder
	constructor: ->
		@data = {}
		@keys = []
		@newLapData = {}
		@numFlips = 0

	load: (fname) ->
		try
			s = fs.readFileSync fname, 'ascii'
			@data = JSON.parse s
			@keys = _.sortBy(_.keys(@data).map((k) -> parseInt k))
			@loaded = true
		catch err
			log "Failed reading #{fname}, err=#{err}"

	save: ->
		if not _.isEmpty @newLapData
			d = @newLapData
		else
			d = @data

		try
			fs.writeFile 'track.lastlap', JSON.stringify(d)
		catch err
			log "Failed to write track.lastlap, err=#{err}"

	key: (tick, offset) ->
		tick.idx * 100 + Math.floor(tick.dst) + (offset or 0)

	add: (tick) ->
		@data[@key tick] =
			speed: tick.speed
			carAngle: tick.carAngle

	addNew: (tick) ->
		@newLapData[@key tick] =
			speed: tick.speed
			carAngle: tick.carAngle

	flip: ->
		if @numFlips++ is 0
			@data = @newLapData
			@keys = _.sortBy(_.keys(@data).map((k) -> parseInt k))
		@newLapData = {}

	get: (tick, offset) ->
		key = @key tick, offset
		k = _.detect @keys, (n) -> n - key >= 0
		@data[k]

exports.Recorder = Recorder
