#
# hwo2014
# jusu@pinktwins.com
#
# - drive a constant-speed-lap, recording speed and carAngle for each tick
# - use the recorded carAngle as braking input
#
# This simple approach should work on all tracks, giving somewhat decent lap times
# without any track-specific optimizations.
#
# It should also survive switching lanes randomly.
#

_          = require 'underscore'
os         = require 'os'
fs         = require 'fs'

{ Recorder } = require './recorder'
{ TickData } = require './tickData'
info         = require './trackInfo'
comms        = require './comms'

botName = process.argv[4]
botKey  = process.argv[5]

send = comms.send
log = console.log

recorder = new Recorder()

pieces = []
lanePoints = []
laneIdx = 0
thHistory = []
isTurboAvail = false

getPiece = (n) -> pieces[n % pieces.length]

tick = new TickData botName

shout = (->
	n = 0
	db = [
		"Don't flip over!"
		"Caking my pants right now.."
		"Again? Oh no.."
		"Here goes!"
		"Cheers!"
		"eat that!"
	]
	->
		n = ++n % db.length
		db[n]
)()

comms.enable
	gameInit: (data) ->
		log "gameInit id=#{data.data.race.track.id} gameId=#{data.gameId}"

		info.set data.data.race.track.id
		recorder.load info.baseRecording
		notLoaded = not recorder.loaded

		pieces = data.data.race.track.pieces
		pieces.forEach (p, n) ->
			if p.switch
				lanePoints.push n-1

			if notLoaded
				idx = n
				dst = 0
				carAngle = parseInt(p.angle) or 0
				speed = (info.constantSpeed or 5) - (Math.abs(carAngle) / 45)
				recorder.addNew { idx, dst, speed, carAngle }

		if notLoaded
			recorder.flip()

	lapFinished: (data) ->
		log "LAP #{JSON.stringify data}"

		if data.data.car.name is botName
			lap = data.data.lapTime.lap

			recorder.save()
			recorder.flip()

			fs.writeFile "throttle.lap.#{lap}", thHistory.join('\n')

		thHistory = []

	carPositions: (data) ->

		tick.set data

		if tick.newIdx()
			if tick.idx in lanePoints
				send
					msgType: 'switchLane'
					data: info.preferredLanes[laneIdx]

				laneIdx = ++laneIdx % info.preferredLanes.length

		if isTurboAvail
			# Should use turbo's duration in this look-ahead
			sumAnglesAhead = [0..4].reduce (sum, n) ->
				sum + Math.abs(getPiece(tick.idx + n)?.angle or 0)

			if tick.carAngleAbs < 10 and sumAnglesAhead <= 10
				s = "Turbo boost! #{shout()}"
				send
					msgType: 'turbo'
					data: s
				log s
				isTurboAvail = false

		throttle = 1.0
		braking = 0

		if not tick.speed?
			tick.speed = info.constantSpeed - 0.01

		recorder.addNew tick

		rec = recorder.get tick, (offset = 100)
		if rec
			speedDiff = tick.speed - rec.speed

			if speedDiff > -1.0
				braking = tick.carAngleAbs / 35.0 + Math.abs(rec.carAngle) / 35.0

				if speedDiff < 1.0 and tick.idx > 6
					mul = 4
				else
					mul = 8

				braking *= (Math.abs(speedDiff) * mul)

				throttle = 1.0 - braking

				# push it!
				if tick.carAngleAbs < 20 and getPiece(tick.idx+1).angle isnt 45 and getPiece(tick.idx+2).angle isnt 45
					boost = 1.0 * ((60 - tick.maxAngle) / 60.0)
					throttle += boost

		# Baby on board!
		if tick.carAngleAbs > 40 or Math.abs(rec?.carAngle) > 40
			if (tick.carAngleAbs > tick.prevCarAngleAbs)
				throttle = 0

		# my moment of shame
		if info.name is 'france' and tick.idx in [12,13,14,22] then throttle -= 0.5

		if throttle < 0.0 then throttle = 0
		if throttle > 1.0 then throttle = 1

		tick.done()

		send
			msgType: 'throttle'
			data: throttle

		#log "throttle #{throttle} braking #{braking} recAngle #{rec?.carAngle or '-'}"
		thHistory.push throttle

	crash: ->
		log "Oh ... crashed."

	join: ->
		console.log 'Joined'

	gameStart: ->
		console.log 'Race started'
		laneIdx = 0
		thHistory = []

	gameEnd: ->
		console.log 'Race ended'

	turboAvailable: (data) ->
		log "got Turbo! #{JSON.stringify data}"
		isTurboAvail = true

, ->
	if os.hostname() is 'illuminati.local'
		# keimola germany usa franceOK elaeintarha imola england suzuka
		send
			msgType: 'joinRace'
			data:
				botId:
					name: botName
					key: botKey
				trackName: 'england'
				carCount: 1
	else
		send
			msgType: 'join'
			data:
				name: botName
				key: botKey
