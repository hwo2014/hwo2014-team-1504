#
# tickData.coffee -- gather my tick data from carPosition message
#

_ = require 'underscore'
os = require 'os'

log = console.log

isLocal = os.hostname() is 'illuminati.local'

class TickData
	constructor: (@botName) ->
		@prevIdx = -1
		@prevDst = -1
		@prevSpeed = -1
		@angleHistory = []
		@maxAngleHistory = 200

	set: (data) ->
		me = _.detect data.data, (d) => d.id.name is @botName

		if not me
			log "Failed to find botName #{@botName} in tickdata."
			return

		@idx = me.piecePosition.pieceIndex
		@dst = me.piecePosition.inPieceDistance
		@carAngle = me.angle
		@carAngleAbs = Math.abs me.angle

		if @idx is @prevIdx
			@speed = @dst - @prevDst

		@gameTick = data.gameTick

		@angleHistory.unshift @carAngleAbs
		if @angleHistory.length >= @maxAngleHistory
			@angleHistory.pop()
		@maxAngle = _.max @angleHistory

		if isLocal
			log "tick idx #{@idx} dst #{@dst} speed #{@speed} carAngle #{@carAngle} maxAngle #{@maxAngle} t #{@gameTick}"

	done: ->
		@prevIdx = @idx
		@prevDst = @dst
		@prevSpeed = @speed
		@prevCarAngle = @carAngle
		@prevCarAngleAbs = @carAngleAbs

	newIdx: ->
		@idx isnt @prevIdx

exports.TickData = TickData
