#
# trackInfo.coffee
#

_ = require 'underscore'

info =
	keimola:
		constantSpeed: 6.5
		baseRecording: 'track.keimola.constantspeed'
		preferredLanes: ['Right', 'Left', 'Left', 'Right', 'Right', 'Right', 'Right']
	germany:
		constantSpeed: 4.5
		baseRecording: 'track.germany.constantspeed'
		preferredLanes: ['Left', 'Right', 'Right', 'Left', 'Right', 'Right', 'Right']
	usa:
		constantSpeed: 9.5
		baseRecording: 'track.usa.v1'
		preferredLanes: ['Right, Right, Left, Right, Left, Right']
	france:
		constantSpeed: 5.0
		baseRecording: 'track.france.v1'
		preferredLanes: ['Left', 'Left', 'Right']
	elaeintarha:
		constantSpeed: 6.0
		baseRecording: 'track.elaeintarha.v1'
		preferredLanes: ['Left', 'Left', 'Right']
	imola:
		constantSpeed: 4.5
		baseRecording: 'track.imola.v1'
		preferredLanes: ['Left', 'Left', 'Right']
	england:
		constantSpeed: 4.5
		baseRecording: 'track.england.v1'
		preferredLanes: ['Left', 'Left', 'Right']
	suzuka:
		constantSpeed: 5.0
		baseRecording: 'track.suzuka.v1'
		preferredLanes: ['Left', 'Right', 'Right']
	defaults:
		constantSpeed: 4.5
		baseRecording: 'none'
		preferredLanes: ['Left', 'Left', 'Right']

exports.set = (trackName) ->
	if info[trackName]
		_.extend exports, info[trackName]
	else
		_.extend exports, info.defaults

	exports.name = trackName
